import React from 'react';
import ReactDOM from 'react-dom';
import './assets/styles/index.scss';
import {Provider} from 'react-redux';
import store from './state-management/store';
import App from './app';

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
document.getElementById('root'));
