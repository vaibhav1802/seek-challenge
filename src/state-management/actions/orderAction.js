import { ADD_ORDER, ADD_CUSTOMER_INFO } from '../constants';

export const addOrder = function(data) {
  return {
    type: ADD_ORDER,
    payload: data
  }
}

export const addCustomerInfo = function(data) {
  return {
    type: ADD_CUSTOMER_INFO,
    payload: data
  }
}