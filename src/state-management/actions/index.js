import { bindActionCreators } from 'redux';
import * as orderObj from './orderAction';
import store from '../store';

const actions = bindActionCreators({
 ...orderObj
}, store.dispatch);

export default actions;