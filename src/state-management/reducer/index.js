import { ADD_ORDER, ADD_CUSTOMER_INFO } from '../constants';
import order from '../state';

export default function (state = order, action) {
  switch (action.type) {
    case ADD_ORDER:
      return Object.assign({}, state, {
        orderObj: action.payload
      })
    case ADD_CUSTOMER_INFO:
      return Object.assign({}, state, {
        customerInfo: action.payload
      })
    default: 
      return state;
  }
}
