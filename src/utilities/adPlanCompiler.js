import {concat, uniq} from 'lodash';

export function adPlanCompiler(planDetails) {
  let exclusiveAdCollection = [];
  let regularAdCollection = [];
 
  if (planDetails.exclusivePricingPlan.length) {
    planDetails.regularPricingPlan.forEach((regularAd) => {
      planDetails.exclusivePricingPlan.forEach((exclusiveAd)=> {
        if (regularAd.adType === exclusiveAd.adType) {
          exclusiveAdCollection.push(exclusiveAd)
        }  else {
          regularAdCollection.push(regularAd)
        }
       });
    });

  // concat the two collection if length of exclusiveAdCollection is less than regularPricingPlan 
  // regularPricingPlan will always have the all possible ad type
  if (exclusiveAdCollection.length < planDetails.regularPricingPlan.length) {
    return concat(exclusiveAdCollection, uniq(regularAdCollection))
  }
    return exclusiveAdCollection;
  }

  return null;
}