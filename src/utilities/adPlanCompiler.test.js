import {adPlanCompiler} from './adPlanCompiler';
describe('Test for adPlanCompiler utility', () => {
  test('Check with valid data and one exclusive plan to function', () => {
    const planDetails = { 
      "customerId" : "11003",
      "customerName": "Axil Coffee Roasters",
      "customerAdd": "Somewhere in West Mel",
      "isExclusivePricing": true,
      "regularPricingPlan": [
        {
          "planType": "regular",
          "adType": "ad_classic",
          "adName": "Classic Ad",
          "description": "Offers the most basic level of advertisement",
          "currency": "AUD",
          "baseAdCount": 0,
          "freeAdCount": 0,
          "originalPrice": 269.99,
          "discountedPrice": 0
        },
        {
          "planType": "regular",
          "adType": "ad_stand_out",
          "adName": "Stand Out Ad",
          "description": "Allows advertisers to use a company logo and use a longer presentation text",
          "currency": "AUD",
          "baseAdCount": 0,
          "freeAdCount": 0,
          "originalPrice": 322.99,
          "discountedPrice": 0
        },
        {
          "planType": "regular",
          "adType": "ad_premium",
          "adName": "Premium Ad",
          "description": "Same benefits as Standout Ad, but also puts the advertisement at the top of the results, allowing higher visibility",
          "currency": "AUD",
          "baseAdCount": 0,
          "freeAdCount": 0,
          "originalPrice": 394.99,
          "discountedPrice": 0
        }
      ],
      "exclusivePricingPlan": [
        {
          "planType": "discounted",
          "adType": "ad_stand_out",
          "adName": "Stand Out Ad",
          "description": "Allows advertisers to use a company logo and use a longer presentation text",
          "currency": "AUD",
          "baseAdCount": 0,
          "freeAdCount": 0,
          "originalPrice": 322.99,
          "discountedPrice": 299.99
        }
      ]
    };
    expect(adPlanCompiler(planDetails)).toHaveLength(3);
  });
});