import config from '../config/config.json';

export class CalculateOrder {
  // Calculate the Classic Ad price
  getClassicPrice(orderObj, exclusivePlan, regularPlan) {
    if (orderObj.length) {
      let classicAdName = config.adTypes.classic.name;
      let classicAd = this.filterAdPlanFromOrder(orderObj, classicAdName);
      let adType = classicAd.name;
      let planType = classicAd.planType;
      let classicAdCount = classicAd.value;
      switch (planType) {
        case config.planTypes.discounted:
          let discountPlan = exclusivePlan.filter((plan) => plan.adType === adType);
          return (discountPlan[0].discountedPrice * classicAdCount).toFixed(2);
        case config.planTypes.dealExtra:
          let dealExtraPlan = exclusivePlan.filter((plan) => plan.adType === adType);
          this.regularClassicPlanPrice = this.getRegularPlanPrice(regularPlan, classicAdName);
          return this.calculatePriceWhenDealExtra(dealExtraPlan[0].baseAdCount, dealExtraPlan[0].freeAdCount, this.regularClassicPlanPrice, classicAdCount);
        case config.planTypes.regular:
          let regularPlanAd = regularPlan.filter((plan) => plan.adType === adType);
          return (regularPlanAd[0].originalPrice * classicAdCount).toFixed(2);
        default:
          break;
      }
    }
  }

  // Calculate the StandOut Ad price
  getStandOutPrice(orderObj, exclusivePlan, regularPlan) {
    if (orderObj.length) {
      let standOutAdName = config.adTypes.standOut.name;
      let standOutAd = this.filterAdPlanFromOrder(orderObj, standOutAdName);
      let adType = standOutAd.name;
      let planType = standOutAd.planType;
      let standOutAdCount = standOutAd.value;
      switch (planType) {
        case config.planTypes.discounted:
          let discountPlan = exclusivePlan.filter((plan) => plan.adType === adType);
          return (discountPlan[0].discountedPrice * standOutAdCount).toFixed(2);
        case config.planTypes.dealExtra:
          let dealExtraPlan = exclusivePlan.filter((plan) => plan.adType === adType);
          this.regularStandOutPlanPrice = this.getRegularPlanPrice(regularPlan, standOutAdName);
          return this.calculatePriceWhenDealExtra(dealExtraPlan[0].baseAdCount, dealExtraPlan[0].freeAdCount, this.regularStandOutPlanPrice, standOutAdCount);
        case config.planTypes.regular:
          let regularPlanAd = regularPlan.filter((plan) => plan.adType === adType);
          return (regularPlanAd[0].originalPrice * standOutAdCount).toFixed(2);
        default:
          break;
      }
    }
  }

  // Calculate the Premium Ad price
  getPremiumPrice(orderObj, exclusivePlan, regularPlan) {
    if (orderObj.length) {
      let premiumAdName = config.adTypes.premium.name;
      let premiumAd = this.filterAdPlanFromOrder(orderObj, premiumAdName);
      let adType = premiumAd.name;
      let planType = premiumAd.planType;
      let premiumAdCount = premiumAd.value;
      switch (planType) {
        case config.planTypes.discounted:
          let discountPlan = exclusivePlan.filter((plan) => plan.adType === adType);
          return (discountPlan[0].discountedPrice * premiumAdCount).toFixed(2);
        case config.planTypes.dealExtra:
          let dealExtraPlan = exclusivePlan.filter((plan) => plan.adType === adType);
          this.regularPremiumPlanPrice = this.getRegularPlanPrice(regularPlan, premiumAdName);
          return this.calculatePriceWhenDealExtra(dealExtraPlan[0].baseAdCount, dealExtraPlan[0].freeAdCount, this.regularPremiumPlanPrice, premiumAdCount);
        case config.planTypes.regular:
          let regularPlanAd = regularPlan.filter((plan) => plan.adType === adType);
          return (regularPlanAd[0].originalPrice * premiumAdCount).toFixed(2);
        default:
          break;
      }
    }
  }
  
  // Method to calculate the price when the deal offer type is extra, e.g get 5 for 3
  calculatePriceWhenDealExtra(baseAdCount, freeAdCount, regularPlanPrice, adCountSelected) {
    if (adCountSelected - freeAdCount < 0) {
      return (adCountSelected * regularPlanPrice).toFixed(2);
    } else if (adCountSelected - freeAdCount === 0) {
      return (baseAdCount * regularPlanPrice).toFixed(2);
    } else {
      return ((adCountSelected - freeAdCount) * regularPlanPrice + (baseAdCount * regularPlanPrice)).toFixed(2); 
    }
  }

  //Method to filter out ad plan and the count of plan selected by the user based on adType passed argument
  filterAdPlanFromOrder(orderObj, adName) {
    let filteredPlanFromOrder = orderObj.filter((order) => order.name === adName);
    return filteredPlanFromOrder[0];
  }

  // Method to calculate price of regular price of an ad type passed to the argument
  getRegularPlanPrice(regularPlan, adType) {
    let filteredRegularPlan = regularPlan.filter((regPlan) => regPlan.adType === adType);
    return filteredRegularPlan[0].originalPrice;
  }
}