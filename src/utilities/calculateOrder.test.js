import {CalculateOrder} from '../utilities/calculateOrder';
import customer1 from '../mock-data/customer_1.json';

const calculateOrderInstance = new CalculateOrder();

test('filterAdPlanFromOrder test for Classic Ad', () => {
  const orderObj = [
    {name: "ad_stand_out", value: 5, planType: "regular"},
    {name: "ad_premium", value: 0, planType: "regular"},
    {name: "ad_classic", value: 3, planType: "discounted"}
  ];
  const adName = "ad_classic";
  expect(calculateOrderInstance.filterAdPlanFromOrder(orderObj, adName)).toEqual({name: "ad_classic", value: 3, planType: "discounted"});
});

test('filterAdPlanFromOrder test for Premium Ad', () => {
  const orderObj = [
    {name: "ad_stand_out", value: 5, planType: "regular"},
    {name: "ad_premium", value: 0, planType: "regular"},
    {name: "ad_classic", value: 3, planType: "discounted"}
  ];
  const adName = "ad_premium";
  expect(calculateOrderInstance.filterAdPlanFromOrder(orderObj, adName)).toEqual({name: "ad_premium", value: 0, planType: "regular"});
});

test('getRegularPlanPrice test for Standout Ad', () => {
  const adType = "ad_stand_out";
  const regularPlan = customer1.regularPricingPlan;
  expect(calculateOrderInstance.getRegularPlanPrice(regularPlan, adType)).toEqual(322.99);
});

test('getRegularPlanPrice test for Premium Ad', () => {
  const adType = "ad_premium";
  const regularPlan = customer1.regularPricingPlan;
  expect(calculateOrderInstance.getRegularPlanPrice(regularPlan, adType)).toEqual(394.99);
});

test('Test for getClassicPrice with discounted plan - Ad classic', () => {
  const orderObj = [
    {name: "ad_stand_out", value: 5, planType: "regular"},
    {name: "ad_premium", value: 0, planType: "regular"},
    {name: "ad_classic", value: 3, planType: "discounted"}
  ];
  const exclusivePlan = customer1.exclusivePricingPlan;
  const regularPlan = customer1.regularPricingPlan;
  expect(calculateOrderInstance.getClassicPrice(orderObj, exclusivePlan, regularPlan)).toEqual("755.97");
});

test('Test for getClassicPrice with regular plan - Ad Classic', () => {
  const orderObj = [
    {name: "ad_stand_out", value: 5, planType: "regular"},
    {name: "ad_premium", value: 0, planType: "regular"},
    {name: "ad_classic", value: 3, planType: "regular"}
  ];
  const exclusivePlan = customer1.exclusivePricingPlan;
  const regularPlan = customer1.regularPricingPlan;
  expect(calculateOrderInstance.getClassicPrice(orderObj, exclusivePlan, regularPlan)).toEqual("809.97");
});


test('Test for getClassicPrice with deal extra plan - Ad Classic', () => {
  const orderObj = [
    {name: "ad_stand_out", value: 5, planType: "regular"},
    {name: "ad_premium", value: 0, planType: "regular"},
    {name: "ad_classic", value: 6, planType: "deal_extra"}
  ];
  const exclusivePlan = customer1.exclusivePricingPlan;
  const regularPlan = customer1.regularPricingPlan;
  expect(calculateOrderInstance.getClassicPrice(orderObj, exclusivePlan, regularPlan)).toEqual("1619.94");
});