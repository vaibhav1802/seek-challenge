import React from 'react';
import {Route, Redirect, Switch} from 'react-router-dom';
import Checkout from '../pages/Checkout';
import OrderSummary from '../pages/OrderSummary';

const applicationRoutes = (
  <Switch>
    <Route name="checkout" path="/checkout" component={Checkout} />
    <Route name="summary" path="/summary" component={OrderSummary} />
    <Redirect exact from="/" to="checkout" />
  </Switch>
);

export default applicationRoutes;
