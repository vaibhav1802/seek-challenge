// In real world scenario, information about the pricing plan will come through the api call after the 
// the customer logs in with unique id
import customer1 from '../mock-data/customer_1'
import customer2 from '../mock-data/customer_2';
import customer3 from '../mock-data/customer_3';
import customer4 from '../mock-data/customer_4';

export function getCustomerPlanDetails(customerId) {
  switch(customerId) {
    case "11001":
      return customer1;
    case "11002":
      return customer2;
    case "11003":
      return customer3;
    default:
      return customer4;
  }
}