import React, { Component } from 'react';
import Select from './../Select';
import customer1 from '../../mock-data/customer_1';
import customer2 from '../../mock-data/customer_2';
import customer3 from '../../mock-data/customer_3';
import customer4 from '../../mock-data/customer_4';
import { default as eventHub }  from '../../eventHub';

export default class Dropdown extends Component {
  constructor(props) {
    super(props);
    this.customerDetails = [customer1, customer2, customer3, customer4];
    this.state = {
      isExclusiveCustomerSelected: this.customerDetails[0].isExclusivePricing
    }
    this.emitter = eventHub.getInstance();
  }

  // Emit the event when the customer is selected from the dropdown for display ad component
  onSelectChange(event) {
    let customerId = event.target.value;
    let selectedCustomer = this.customerDetails.filter((customer) => customer.customerId === customerId);
    this.setExclusiveMessage(selectedCustomer[0]);
    this.emitter.emit('CLIENT_SELECTED', customerId);
  }

  setExclusiveMessage(selectedCustomer) {
    if (selectedCustomer.isExclusivePricing) {
      this.setState({
        isExclusiveCustomerSelected: true
      });
      return;
    } 

    this.setState({
      isExclusiveCustomerSelected: false
    });
  }

  render() { 
    let isExclusive = this.state.isExclusiveCustomerSelected;
    return(
      <div className="dropdown-wrapper">
        <Select items={this.customerDetails} customClass={"customer-dd"} onSelectChange={(e) => this.onSelectChange(e)} />
        <p className="exclusive-txt">{isExclusive ? 'Exclusive Customer': null}</p>
      </div>
    );
  }
}
