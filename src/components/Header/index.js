import React from 'react';
import PropTypes from 'prop-types';

const Header = props => {
  return (
    <header className="header-wrapper">
      <div className="header-items">
        <a className="logo-wrapper" href="#order">
          <img className="img-logo" src={props.logo} alt="logo-seek"></img>
        </a>
      </div>
    </header>
  )
};

Header.propTypes = {
  logo: PropTypes.string
};

export default Header;