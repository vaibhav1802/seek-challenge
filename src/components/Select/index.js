import React, { Component } from 'react';
import PropTypes from 'prop-types';

export class Select extends Component {
  render() {  
   const {items, onSelectChange} = this.props;
    return(
      <div className="select-wrapper">
        <select className="select" onChange={onSelectChange}>
          {
            items.map((item, count) => 
              <option key={count} value={item.customerId}>{item.customerName}</option>
            )
          }
        </select>
      </div>
    );
  }
}

Select.propTypes = {
  items: PropTypes.array,
  onSelectChange: PropTypes.func
};

export default Select;
