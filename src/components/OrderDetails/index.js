import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {CalculateOrder} from '../../utilities/calculateOrder';
import config from '../../config/config.json';

export default class OrderDetails extends Component {
  constructor(props) {
    super(props);
    this.calculateOrder = new CalculateOrder();
    this.orderObj = this.props.orderObj;
    this.exclusivePlan = this.props.customerInfo.exclusivePricingPlan;
    this.regularPlan = this.props.customerInfo.regularPricingPlan;
  }

  calculateEachAdTotal() {
    this.classicAdTotal = this.calculateOrder.getClassicPrice(this.orderObj, this.exclusivePlan, this.regularPlan);
    this.standOutAdTotal = this.calculateOrder.getStandOutPrice(this.orderObj, this.exclusivePlan, this.regularPlan);
    this.premiumAdTotal = this.calculateOrder.getPremiumPrice(this.orderObj, this.exclusivePlan, this.regularPlan);
  }

  calculateFinalTotal() {
    return (Number(this.classicAdTotal) + Number(this.standOutAdTotal) + Number(this.premiumAdTotal)).toFixed(2);
  }

  getSelectedCountForAd(adName) {
    if (this.orderObj.length) {
      let filteredAd = this.orderObj.filter((ad) => ad.name === adName);
      return filteredAd[0].value;
    }
  }

  render() {
    this.calculateEachAdTotal();
    
    return(
      <div className="order-summary">
        <div className={`item-order-summary ${config.adTypes.classic.name}`}>
          <span>{config.adTypes.classic.label} :</span> 
          <p>Ad Selected: {this.getSelectedCountForAd(config.adTypes.classic.name)}</p>
          <p>Amount: ${this.classicAdTotal}</p>
        </div>
        <div className={`item-order-summary ${config.adTypes.standOut.name}`}>
          <span>{config.adTypes.standOut.label} : </span>
          <p>Ad Selected: {this.getSelectedCountForAd(config.adTypes.standOut.name)}</p>
          <p>Amount: ${this.standOutAdTotal}</p>
        </div>
        <div className={`item-order-summary ${config.adTypes.premium.name}`}>
          <span>{config.adTypes.premium.label} : </span>
          <p>Ad Selected: {this.getSelectedCountForAd(config.adTypes.premium.name)}</p>
          <p>Amount: ${this.premiumAdTotal}</p>
        </div>
        <div className="final-total">
          <span className="label">Total: </span>
          <span className="sum">${this.calculateFinalTotal()}</span>
        </div>
      </div>
    );
  }
}

OrderDetails.propTypes = {
  orderObj: PropTypes.array,
  customerInfo: PropTypes.object
};
