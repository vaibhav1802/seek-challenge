import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AdCount from '../AdCount';
import {adPlanCompiler} from '../../utilities/adPlanCompiler';
import config from '../../config/config';

class DisplayAds extends Component {
  displayExclusiveLabel(ad) {
    let isExclusiveAd =  ad.planType === 'discounted' || ad.planType === 'deal_extra';
    return(
      <span className="exclusiveLabel">{isExclusiveAd ? 'Exclusive' : null}</span>
    )
  }

  // Only display exlusive deal and not regular
  displayDeal(ad) {
    if (ad.planType !== config.planTypes.regular) {
      switch(ad.planType) {
        case config.planTypes.discounted :
          return (
            <span className="exclusive-deal">{`Original Price is $${ad.originalPrice} but you are getting an exclusive deal at $${ad.discountedPrice}`}</span>
          );
        case config.planTypes.dealExtra :
          return (
            <span className="exclusive-deal">{`Get ${ad.freeAdCount} ads at the price of ${ad.baseAdCount}`}</span>
          );
          default:
            break;
      }
    }
  }

  render() {
    let customerPlanDetails = this.props.planDetails;
    if (customerPlanDetails && customerPlanDetails.isExclusivePricing) {
      let filteredCustomerAd = adPlanCompiler(customerPlanDetails);
      this.adItems = filteredCustomerAd;
    } 
    else {
      this.adItems = customerPlanDetails.regularPricingPlan; 
    }
  
    return (
      <div className="display-ads-wrapper">
        <div className="display-ads">
          {
            this.adItems.length && 
            this.adItems.map((ad, index) => 
            <div id={ad.adType} className="ad-item" key={index}>
              <div className="ad-info">
                <h4 className="adName">{ad.adName}</h4>
                <p className="adDesc">{ad.description}</p>
                {this.displayExclusiveLabel(ad)}
                <p>{this.displayDeal(ad)}</p>
              </div>
              <AdCount planType={ad.planType} adType={ad.adType}/>
            </div>
            )
          }
        </div>
      </div>
    )
  }
};

DisplayAds.propTypes = {
  planDetails: PropTypes.object
};

export default DisplayAds;