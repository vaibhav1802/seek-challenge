import React, {Component} from 'react';
import PropTypes from 'prop-types';

export default class AdCount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      adCount: 0
    };
  }

  onCountChange(event) {
    this.setState({
      adCount: event.currentTarget.value
    });
  }

  render() {
    const {adType, planType} = this.props;
    return(
      <div className="count-text-wrapper">
        <input name={adType} value={this.state.adCount}
          onChange={(e) => this.onCountChange(e)}
          id={planType}
          className="add-counter"  
          type="number" min="0" max="20">
        </input>
      </div>
    )
  }
}

AdCount.propTypes = {
  adType: PropTypes.string,
  planType: PropTypes.string
};
