import React from 'react';
import { mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import AdCount from './index.js';

configure({ adapter: new Adapter() });

let saved = {};

describe('<AdCount />', () => {
  beforeEach(() => {
    saved = {
      spies: {
        onCountChange: jest.spyOn(AdCount.prototype, 'onCountChange')
      },
      wrapper: mount(<AdCount adType={'premium'} planType={'regular'}/>)
    };
  });
  afterEach(() => {
    saved.spies.onCountChange.mockRestore();
    saved.wrapper.unmount();
    saved = {};
  });
  describe('Interactions with ad count box', () => {
    test('When ad count change event is triggered', () => {
      saved.wrapper.find('.add-counter').simulate('change');
      expect(saved.spies.onCountChange).toHaveBeenCalledTimes(1);
    });
  });
});