import React, { Component } from 'react';
import OrderDetails from '../../components/OrderDetails';
import { connect } from 'react-redux';

class OrderSummary extends Component {
  render() {
    const {orderObj, customerInfo} = this.props;
    return(
      <div id='order-summary'>
        <h1 className="page-title">Order Summary</h1>
        <OrderDetails orderObj={orderObj} customerInfo={customerInfo}/>
      </div>
    );
  }
};

 // Bind redux data to component
 const mapStateToProps = (state, props) => {
  return {
    orderObj: state.orderObj,
    customerInfo: state.customerInfo
  };
}

export default connect(mapStateToProps)(OrderSummary);