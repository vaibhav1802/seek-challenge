import React, { Component } from 'react';
import DisplayAds from '../../components/DisplayAds';
import { default as eventHub }  from '../../eventHub';
import { getCustomerPlanDetails } from '../../services/getCustomerPlanDetails';
import customer1 from '../../mock-data/customer_1';
import actions from '../../state-management/actions';
import Dropdown from '../../components/Dropdown';

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      planDetails: customer1
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  // Setup the event listener when the client is selected from the dropdown in the header component
  componentDidMount() {
    this.emitter = eventHub.getInstance();
    this.notificationListener = this.emitter.addListener('CLIENT_SELECTED', (option) => this.clientSelected(option));
  }

  clientSelected(customerId) {
    this.setState({
      planDetails: getCustomerPlanDetails(customerId)
    });
  }


  handleSubmit(event) {
    event.preventDefault();
    const form = event.target;
    const data = new FormData(form);
    let adCountSelected = [];
  
    for (let name of data.keys()) {
      let adSelectedObj = {};
      const input = form.elements[name];

      adSelectedObj.name = input.name;
      adSelectedObj.value = input.value;
      adSelectedObj.planType = input.id;
      adCountSelected.push(adSelectedObj);
    }
    
    // Dispatch the order data and selected customer to redux store
    actions.addOrder(adCountSelected);
    actions.addCustomerInfo(this.state.planDetails);
    window.location.hash="summary";
  }

  render() {
    return(
      <div id='checkout'>
        <Dropdown />
        <h1 className="page-title">Checkout</h1>
        <form onSubmit={this.handleSubmit}>
          <DisplayAds planDetails={this.state.planDetails}/>
          <input className="btn checkout" type="submit" value="ORDER"></input>
        </form>
      </div>
    );
  }
};