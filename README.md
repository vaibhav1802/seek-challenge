SEEK CHALLENGE
ASSUMPTIONS:
1. Since there was no mock data provided, I have created the response data structure for 4 customers. One customer has Regular Plan and 3 are having Exclusive Plans.

2. The mock customer data will be ideally fetched from the backend once the user logs in using the credentials.

3. isExclusivePricing flag will be an important flag to figure out if customer has the exclusive pricing plan

THINGS THAT COULD HAVE BEEN IMPROVED IF TIME ALLOWS:
1. More Unit Tests
2. Better Order Summary Page
3. Better Styling

PROJECT DEPENDENCIES:
1. React Router: For routing

2. Jest and Enzyme: For unit test

3. FB Emitter - Event
To trigger the change event from the dropdown when a customer is selected using dropdown.

4. Polyfill native fetch using whatwg-fetch 
In the example, I have used mock data, so the whole implementation of promise couldn't be done.

5. Proptypes for typechecking

6. Node sass for sass style compiler

7. Redux - For storing the Ad count selected by the user and customer info to pass that to Order Summary Page


This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## PROJECT REQUIREMENTS
Coding Exercise
We are interested in seeing your coding, problem solving and collaboration style. We would like you to build the following system. When you come to meet us for your programming interview, bring along your laptop or files, as you will be pairing with a SEEKer who will work with you on your solution to complete a stretch goal.
For the purpose of this exercise, SEEK is in the process of rewriting its job ads checkout system. We want to offer different products to recruiters:
Name
Description
Retail Price
Classic Ad
Offers the most basic level of advertisement
$269.99
Stand out Ad
Allows advertisers to use a company logo and use a longer presentation text
$322.99
Premium Ad
Same benefits as Standout Ad, but also puts the advertisement at the top of the results, allowing higher visibility
$394.99
We have established a number of special pricing rules for a small number of privileged customers: 1. SecondBite
- Gets a 3​ for 2​ deal on ​Classic Ads 2. Axil Coffee Roasters
- Getsadiscounton​StandoutAds​wherethepricedropsto$​ 299.99p​ erad
3. MYER
- Gets a 5​ for 4​ deal on ​Stand out Ads
- Getsadiscounton​PremiumAds​wherethepricedropsto$​ 389.99​perad

These details are regularly renegotiated, so we need the pricing rules to be as flexible as possible as they can c​ hange​ in the future with little notice.
© SEEK. All rights reserved. Page 1 of 2

The interface to our checkout looks like this pseudocode:
Checkout co = Checkout.new(pricingRules)
co.add(item1)
co.add(item2)
co.total()
Example scenarios
Customer: default
Items: `classic`, `standout`, `premium` Total: $987.97
Customer: SecondBite
Items: `classic`, `classic`, `classic`, `premium` Total: $934.97
Customer: Axil Coffee Roasters
Items: `standout`, `standout`, `standout`, `premium` Total: $1294.96
Tips
We value work-life balance and do not want you to lose a weekend trying to solve this problem. Only spend enough time required to produce an a​ppropriate,​ ​clean​, t​estable​ and ​maintainable​ solution to the stated problem.
You should focus on delivering only a back-end O​​R front-end implementation. Keep it simple.
Please bring your laptop to the interview, or let us know beforehand if you would like us to provide one. Good luck, have fun with it, and we look forward to meeting you soon!

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

